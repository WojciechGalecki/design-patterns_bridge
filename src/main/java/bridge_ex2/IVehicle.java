package bridge_ex2;

public interface IVehicle {

    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();
}
