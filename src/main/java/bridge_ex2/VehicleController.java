package bridge_ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VehicleController {

    private static List<IVehicle> vehicles = new ArrayList<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        vehicles.add(new Car("BMW"));
        vehicles.add(new Boat("Titanic"));
        vehicles.add(new Aircraft("F16"));

        while (true) {
            System.out.println("Give me number (1-4)");
            System.out.println("EXIT -> 5");
            int number = sc.nextInt();
            switch(number){
                case 1 :{
                    for (IVehicle v:vehicles) {
                        v.moveUp();
                    }
                    break;
                }
                case 2:{
                    //for (IVehicle v:vehicles) {
                    //    v.moveDown();
                    //}
                    vehicles.forEach(v ->v.moveDown());
                    break;
                }
                case 3: {
                    for (IVehicle v:vehicles) {
                        v.moveLeft();
                    }
                    break;
                }
                case 4: {
                    for (IVehicle v:vehicles) {
                        v.moveRight();
                    }
                    break;
                }
                case 5: {
                    return;
                }
            }
        }

    }
}
