package bridge_ex2;

public class Aircraft implements IVehicle {

    private String name;

    public Aircraft(String name) {
        this.name = name;
    }

    @Override
    public void moveRight() {
        System.out.println("moveRight " + name);
    }

    @Override
    public void moveLeft() {
        System.out.println("moveLeft " + name);
    }

    @Override
    public void moveUp() {
        System.out.println("moveUp " + name);
    }

    @Override
    public void moveDown() {
        System.out.println("moveDown " + name);
    }
}
