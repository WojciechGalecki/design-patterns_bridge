package bridge_ex1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MyFilePrinter implements IPrinter {

    File file = new File("file.txt");

    public MyFilePrinter(){
    }

    @Override
    public void printMessage(String message) {
        try(PrintWriter pr = new PrintWriter(new FileWriter(file))){
            pr.print(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
