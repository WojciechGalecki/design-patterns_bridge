package bridge_ex1;

public interface IPrinter {

    void printMessage(String message);
}
