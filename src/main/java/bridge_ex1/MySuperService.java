package bridge_ex1;

public class MySuperService {

    private IPrinter myPrinter;


    public MySuperService(IPrinter myPrinter) {
        this.myPrinter = myPrinter;
    }

    public void printData(String data){
        myPrinter.printMessage(data);
    }
}
